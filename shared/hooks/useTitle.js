import { useEffect } from 'react';

const defaultTitle = 'Matchify';

export default (title) => useEffect(() => {
  document.title = `${defaultTitle} | ${title}` || defaultTitle;
}, [title]);
