import { useEffect } from 'react';
import NProgress from 'nprogress';
import Router from 'next/router';

const { done: defaultDone, start: defaultStart } = NProgress;

export default (done = defaultDone, start = defaultStart) => {
  useEffect(() => {
    Router.onRouteChangeStart = start;
    Router.onRouteChangeComplete = done;
    Router.onRouteChangeError = done;
    return () => undefined;
  }, [done, start]);
};
