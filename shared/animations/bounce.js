import { keyframes } from '@emotion/core';

export default keyframes`
  from {
    transform: scale(1.01);
  }
  to {
    transform: scale(0.99);
  }
`;
