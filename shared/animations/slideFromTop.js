import { keyframes } from '@emotion/core';

export default keyframes`
    from {
      transform: translate(-50%, -500px);
    }
    to {
      transform: translate(-50%, -50%);
    }
`;
