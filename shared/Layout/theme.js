export default {
  fontFamily: 'Roboto, sans-serif',
  background: {
    whiteSmoke: '#f9f7f3',
    primary: '#ffd668',
    secondary: '#232a47',
    darkBlue: '#061124',
  },
  color: {
    primary: '#ffd668',
    secondary: '#e4e4e4',
    ivory: '#ffd668',
    whiteSmoke: '#f9f7f3',
    pastelGray: '#c9c9c9',
    blue: '#232a47',
    darkBlue: '#061124',
  },
};
