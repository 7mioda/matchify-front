import React from 'react';
import withStyle from './withStyle';
import useLoader from '../hooks/useLoader';
import Footer from '../../components/footer/Footer';
import Header from '../../components/header/Header';

const Layout = withStyle(({ className, children }) => {
  useLoader();
  return (
    <div className={className}>
      <Header />
      <h1>Matchify</h1>
      { children }
      <div style={{ height: '500px', background: '#fff' }} />
      <Footer />
    </div>
  );
});

export default Layout;
