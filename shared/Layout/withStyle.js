import styled from '@emotion/styled';


export default (component) => styled(component)`
    color: ${({ theme: { color: { secondary } } }) => secondary};
    background-color: ${({ theme: { background: { secondary } } }) => secondary};
    font-family: Roboto, sans-serif;
`;
