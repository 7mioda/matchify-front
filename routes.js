// eslint-disable-next-line no-multi-assign
const routes = module.exports = require('next-routes')();

routes
  .add('index', '/matchify.it', 'index')
  .add('home', '/matchify.it/home', 'home')
  .add('about', '/about', 'about')
  .add('contact', '/matchify.it/contact', 'contact');
