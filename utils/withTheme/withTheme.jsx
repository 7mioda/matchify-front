import React from 'react';
import { ThemeProvider } from 'emotion-theming';

// eslint-disable-next-line func-names
const withTheme = (theme) => (Component) => function () {
  return (
    <ThemeProvider theme={theme}>
      <Component />
    </ThemeProvider>
  );
};

export default withTheme;
