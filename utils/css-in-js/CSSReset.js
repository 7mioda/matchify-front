import { Global } from '@emotion/core';
import React from 'react';
import resetCss from './reset-css';

export default () => (
  <Global
    styles={resetCss}
  />
);
