import { configure, addParameters } from '@storybook/react';
import { themes } from '@storybook/theming';
import responsive from './responsive';


addParameters({
    options: {
        theme: themes.dark,
    },
    viewport: {
        viewports: {
            ...responsive,
        },
    },
});

configure(require.context('../components', true, /\.stories\.js$/), module);
