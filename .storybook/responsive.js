export default {
    desktop: {
        name: 'desktop',
        styles: {
            width: '1024px',
            height: '963px',
        },
    },
    tablet: {
        name: 'tablet',
        styles: {
            width: '768px',
            height: '801px',
        },
    },
    mobile: {
        name: 'mobile',
        styles: {
            width: '320px',
            height: '801px',
        },
    },
};
