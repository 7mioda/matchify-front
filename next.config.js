module.exports = {
  webpack: (config, {
    // eslint-disable-next-line no-unused-vars
    buildId, dev, isServer, defaultLoaders,
  }) => {
    config.module.rules.push({
      test: /\.(graphql|gql)$/,
      use: [
        {
          loader: 'graphql-tag/loader',
        },
      ],
      exclude: /node_modules/,
    });
    return config;
  },
  useFileSystemPublicRoutes: false,
};
