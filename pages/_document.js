// _document is only rendered on the server side and not on the client side
// Event handlers like onClick can't be added to this file

// A custom <Document> is commonly used to augment
// your application's <html>
// and <body> tags. This is necessary because Next.
// js pages skip the definition of the surrounding document's markup.
//
//     This allows you to support Server-Side Rendering for CSS-in-JS libraries
//     like styled-components or emotion. Note, styled-jsx is included in Next.js by default.

// ./pages/_document.js
import Document, {
  Html, Head, Main, NextScript,
} from 'next/document';
import React from 'react';
import CSSReset from '../utils/css-in-js/CSSReset';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <>
        <CSSReset />
        <Head>
          <meta charSet="utf-8" />
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css" />
          <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <Html>
          <body>
            <Main />
            <NextScript />
          </body>
        </Html>
      </>
    );
  }
}

export default MyDocument;
