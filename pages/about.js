import React from 'react';
import Link from 'next/link';
import { withApollo } from '../lib/apollo';


const About = ({ className, useTitle }) => {
  useTitle('about');
  return (
    <div className={className}>
      <Link href="/home">
            home
      </Link>
    </div>
  );
};
export default withApollo(About);
