import React from 'react';
import { withApollo } from '../lib/apollo';

const index = () => (
  <>
    <h4>
    Hello Matchify
    </h4>
  </>
);

export default withApollo(index);
