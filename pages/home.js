import React from 'react';
import Link from 'next/link';
import Users from '../components/Users';
import { withApollo } from '../lib/apollo';

const Home = ({ useTitle }) => {
  useTitle('home');
  return (
    <h4>
            Home Matchify
      <Link href="/about">
                About Us
      </Link>
      <Users />
    </h4>
  );
};

export default withApollo(Home);
