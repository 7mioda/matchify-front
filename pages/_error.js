// 404 or 500 errors are handled both client and server
// side by a default component error.js.
// If you wish to override it, define a _error.js in the pages folder:
//
//     ⚠️ The pages/_error.js component is only used in production.
//     In development you get an error with call stack to know where the error originated from. ⚠️
import React from 'react';

class Error extends React.Component {
  static getInitialProps({ res, err }) {
    // eslint-disable-next-line no-nested-ternary
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

  render() {
    return (
      <p>
        {this.props.statusCode
          ? `An error ${this.props.statusCode} occurred on server`
          : 'An error occurred on client'}
      </p>
    );
  }
}

export default Error;
