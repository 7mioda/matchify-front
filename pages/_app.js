import React from 'react';
import App from 'next/app';
import { ThemeProvider } from 'emotion-theming';
import Layout from '../shared/Layout/Layout';
import theme from '../shared/Layout/theme';

// Next.js uses the App component to initialize pages.
// You can override it and control the page initialization.
// Which allows you to do amazing things like:
//
//     Persisting layout between page changes
// Keeping state when navigating pages
// Custom error handling using componentDidCatch
// Inject additional data into pages (for example by processing GraphQL queries)

class MyApp extends App {
  // Only uncomment this method if you have blocking data requirements for
  // every single page in your application. This disables the ability to
  // perform automatic static optimization, causing every page in your app to
  // be server-side rendered.
  //
  // static async getInitialProps(appContext) {
  //   // calls page's `getInitialProps` and fills `appProps.pageProps`
  //   const appProps = await App.getInitialProps(appContext);
  //
  //   return { ...appProps }
  // }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <ThemeProvider theme={theme}>
        <Layout>
          {
            (useTitle) => (
              // eslint-disable-next-line react/jsx-props-no-spreading
              <Component useTitle={useTitle} {...pageProps} />
            )
          }
        </Layout>
      </ThemeProvider>
    );
  }
}

export default MyApp;
