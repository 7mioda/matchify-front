import React, { useEffect } from 'react';
import Link from "next/link";


const Header = () => {
  const listenScrollEvent = () => console.log(window.scrollY);
  useEffect(() => {
    window.addEventListener('scroll', listenScrollEvent);
    return () => window.removeEventListener('scroll', listenScrollEvent);
  }, []);

  return (
    <header>
      <Link href="/home">
        home
      </Link>
      <Link href="/home">
        home
      </Link>
      <Link href="/home">
        home
      </Link>
    </header>
  );
};

export default Header;
