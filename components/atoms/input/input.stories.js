import React from 'react';
import Input from './Input';

export default { title: 'Input' };

export const highlightedInput = () => (
  <Input
    width="500px"
    padding="0 20px"
    highlighted
    Prefix={(
      <span role="img" aria-label="so cool">
        😀
      </span>
    )}
    Suffix={(
      <span role="img" aria-label="so cool">
        😎
      </span>
    )}
  />
);
