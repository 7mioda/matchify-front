import React, { useState } from 'react';
import Modal from './Modal';
import Button from '../button/Button';


export default { title: 'Modal' };


export const ModalWithText = () => {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <Button onClick={() => setVisible(!visible)}>Open Modal</Button>
      <Modal visible={visible} close={() => setVisible(false)}>
        <div style={{ width: '500px', height: '500px' }}>
          <span role="img" aria-label="so cool">
      😎
          </span>
        </div>
      </Modal>
    </>
  );
};
