import React from 'react';
import Carousel from './Carousel';

export default { title: 'Carousel' };

export const withThreeImages = () => (
  <div style={{ width: '1024px', height: '500px' }}>
    <Carousel itemNumber={3}>
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
    </Carousel>
  </div>
);


export const singleImages = () => (
  <div style={{ width: '1024px', height: '500px' }}>
    <Carousel>
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
    </Carousel>
  </div>
);

export const withIndicators = () => (
  <div style={{ width: '1024px', height: '500px' }}>
    <Carousel indicators>
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
      <img src="https://via.placeholder.com/1024/500" alt="" />
    </Carousel>
  </div>
);
