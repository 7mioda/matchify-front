import React, { useReducer } from 'react';
import { generate } from 'short-id';
import PropTypes from 'prop-types';
import withStyle from './withStyle';

const Rater = ({
  className, rating, maxRate, outlined, filled,
}) => {
  const [state, setState] = useReducer(
    (initState, newState) => ({ ...initState, ...newState }),
    { rating, tempRating: 0 }
  );
  const rate = (newRating) => {
    setState({
      rating: newRating,
      tempRating: rating,
    });
  };

  const hoverStar = (newRating) => {
    const { rating: StateRating } = state;
    setState({
      tempRating: StateRating,
      rating: newRating,
    });
  };

  const leaveStar = () => {
    const { tempRating } = state;
    setState({ rating: tempRating });
  };
  const stars = [1, 2, 4, 5, 6].map((item, index) => {
    const Component = state.rating >= index ? filled : outlined;
    return (
      <Component
        type="button"
        key={generate()}
        onMouseOver={() => hoverStar(index)}
        onMouseOut={leaveStar}
        onClick={() => rate(index)}
        onFocus={() => undefined}
        onBlur={() => undefined}
      />
    );
  });
    console.log(stars);
  return (
    <div className={className}>
      {stars}
    </div>
  );
};


Rater.propTypes = {
  className: PropTypes.string.isRequired,
  rating: PropTypes.number.isRequired,
  maxRate: PropTypes.number.isRequired,
};

export default withStyle(Rater);
