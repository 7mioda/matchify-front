import styled from '@emotion/styled';

export default (component) => styled(component)`
  * {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
  }
  
  .star {
    width: 10px;
    height: 10px;
    margin-left: 1px;
    cursor: pointer;
    border: none;
    outline: none;
    background: url("https://via.placeholder.com/50/50");
  }
  
  .filled {
    background: url("https://via.placeholder.com/50/50");
  }
  
  .outlined {
    background: url("https://via.placeholder.com/50/50");
  }
`;
