import React from 'react';
import Rater from './Rater';
import Button from '../button/Button';

export default { title: 'Rater' };


export const withFiveStar = () => (
  <Rater maxRate="5" rating="1" filled={<Button rounded background="red" />} outlined={<Button rounded background="red" />} />
);
