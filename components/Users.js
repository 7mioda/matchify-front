import { useQuery } from '@apollo/react-hooks';
import USERS_QUERY from '../graphql/queries/users.graphql';

export default () => {
  const {
    loading, error, data,
  } = useQuery(USERS_QUERY);
  console.log(loading, error, data);
  return (
    <div>
      <h1>Test</h1>
    </div>
  );
};
