import React from 'react';
import withStyle from './withStyle';
import Input from '../atoms/input/Input';

const Footer = ({ className }) => (
  <footer className={`${className}`}>
    <section className="footer__newsletter">
      <div className="newsletter__image">
        <img src="https://place-hold.it/300x500" alt=""/>
      </div>
      <div className="newsletter__input">
        <h1>Subscribe our newsletter</h1>
        <Input placeholder="Enter your email here..." classNames={['newsletter__email']} />
      </div>
    </section>
    <section className="footer__container">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      </p>
      <section className="footer__quick-link">
        <h1>quick link</h1>
        <ul>
          <li>About us</li>
          <li>Service</li>
          <li>Teams</li>
          <li>Conditions</li>
          <li>Contact</li>
        </ul>
      </section>
      <section className="footer__quick-link">
        <h1>quick link</h1>
        <ul>
          <li>About us</li>
          <li>Service</li>
          <li>Teams</li>
          <li>Conditions</li>
          <li>Contact</li>
        </ul>
      </section>
      <section className="footer__contact-info">
        <h1>Contact info</h1>
        <div>
          <h3>Address</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </div>
        <div>
          <h3>Phone</h3>
          <p>+00000000000000</p>
          <p>+00000000000000</p>
        </div>
        <div>
          <h3>web</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </div>
      </section>
    </section>
    <section className="footer__copyright">
          @2020 footer copyright
    </section>
  </footer>
);

export default withStyle(Footer);
