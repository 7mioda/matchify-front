import styled from '@emotion/styled';

export default (component) => styled(component)`
    font-size: 1rem;
    background-color: ${({ theme: { background: { darkBlue } } }) => darkBlue};
    overflow: hidden;
    p {
      font-weight: 300;
      line-height: normal;
      color: ${({ theme: { color: { whiteSmoke } } }) => whiteSmoke};
    }
    .footer__newsletter {
        position: absolute;
         left: 50%;
         display: flex;
         transform: translate(-50%, -50%);
         width: 80%;
         max-width: 1100px;
         height: 140px;
         background-color: ${({ theme: { background: { primary } } }) => primary};
      .newsletter__image {
            display: none;
      }
      .newsletter__input {
             width: 100%;
             padding: 20px 30px;
              h1 {
                text-align: center;
                font-weight: 900;
                font-size: 1.5rem;
                color: ${({ theme: { color: { blue } } }) => blue};
              }
              .newsletter__email {

                  padding: 10px 50px;
                  input {
                    background-color: ${({ theme: { background: { primary } } }) => primary};
                    color: ${({ theme: { color: { blue } } }) => blue};
                    border-radius: 0;
                    border: 1px solid ${({ theme: { color: { blue } } }) => blue};
                  }
              }
      }
    }
    .footer__container{
        max-width: 1100px;
        color: ${({ theme: { color: { secondary } } }) => secondary};
        padding: 100px 15px 30px 10px;
        margin-left: auto;
        margin-right: auto;
        &> p {
          margin: 20px 0;
        }
         h1 {
                font-size: 1.5rem;
                font-weight: 700;
                text-transform: uppercase; 
                margin: 20px 0;
              }
         h3 {
              font-weight: 500;
              text-transform: capitalize;
              color: ${({ theme: { color: { whiteSmoke } } }) => whiteSmoke};
              margin: 10px 0;
         }
         .footer__quick-link {
               ul {
                    list-style: square inside;
                    li {
                         margin: 15px 0;
                         font-weight: 300;
                         &:hover {
                          color: ${({ theme: { color: { primary } } }) => primary};
                          cursor: pointer;
                        }
                    }
               } 
         }
     }
    .footer__copyright {
      width: 100%;
      font-weight: 500;
      padding: 10px 15px;
      background-color: ${({ theme: { background: { primary } } }) => primary};
      color: ${({ theme: { color: { blue } } }) => blue};
    }
    
    @media(min-width: 900px) {
         .footer__newsletter {
             position: absolute;
             left: 50%;
             display: flex;
             transform: translate(-50%, -50%);
             width: 80%;
             max-width: 1100px;
             height: 140px;
             background-color: ${({ theme: { background: { primary } } }) => primary};
              .newsletter__image { 
              display: block;
              width: 30%;
              height: 100%;
                img {
                    width: 100%;
                    height: 100%;
                    }
           } 
         }
        .footer__container{
          display: flex;
          justify-content: space-between;
          padding: 100px;  
          &>p {
            width: 30%;
            flex: 0 1 auto;
            padding-top: 2%;
          }
        }
    }
`;
